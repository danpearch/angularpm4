import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  pagination = {
    currentPage: 1,
    itemsPerPage: 10,
    lastPage: null,
    totalItems: null
  }
  userTasks = [];

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.getUserTasks();
  }

  getUserTasks(page?: number, filter?: string, sortBy?: string, sortOrder?: string): void {
    this.userTasks = [];
    this.userService.getUserTasks(page, filter, sortBy, sortOrder).subscribe((response) => {
      this.userTasks = response['data'];
      this.pagination.currentPage = response['meta']['current_page'];
      this.pagination.lastPage = response['meta']['last_page'];
      this.pagination.totalItems = response['meta']['total'];
    },
    (error) => {
      console.error(error);
    })
  }

}
