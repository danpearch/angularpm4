export const environment = {
  production: true,
  apiDomain: 'processmaker.net',
  apiProtocol: 'https://'
};
